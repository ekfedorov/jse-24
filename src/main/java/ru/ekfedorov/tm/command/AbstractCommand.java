package ru.ekfedorov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.enumerated.Role;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public abstract String name();

    @Nullable
    public Role[] roles() {
        return null;
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        if (!isEmpty(name())) result += name();
        if (!isEmpty(arg())) result += " [" + arg() + "]";
        if (!isEmpty(description())) result += " - " + description();
        return result;
    }

}
